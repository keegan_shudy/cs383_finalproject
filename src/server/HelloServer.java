package server;

import java.util.Iterator;
import java.util.Map;

/**
 * An example of subclassing NanoHTTPD to make a custom HTTP server.
 */
public class HelloServer extends NanoHTTPD {
    public HelloServer() {
        super(80);
    }

    @Override public Response serve(IHTTPSession session) {
        Method method = session.getMethod();
        String uri = session.getUri();
        System.out.println(method + " '" + uri + "' ");
        Map<String, String> header = session.getHeaders();
        Map<String, String> parms = session.getParms();

        Iterator<String> e = header.keySet().iterator();
        while (e.hasNext()) {
            String value = e.next();
            System.out.println("  HDR: '" + value + "' = '" + header.get(value) + "'");
        }
        e = parms.keySet().iterator();
        System.out.println(parms.keySet().size() + " parameters specified");
        while (e.hasNext()) {
            String value = e.next();
            System.out.println("  PRM: '" + value + "' = '" + parms.get(value) + "'");
        }

        StringBuilder msg = new StringBuilder("<html><body><h1>Hello server</h1>\n");
        if (parms.get("username") == null) {
        	msg.append("<form action='?' method='get'>\n");
        	msg.append("<p>Your name: <input type='text' name='username'></p>\n");
        	msg.append("</form>\n");
        } else {
            msg.append("<p>Hello, " + parms.get("username") + "!</p>");
        }
        msg.append("</body></html>\n");

        return new NanoHTTPD.Response(msg.toString());
    }


    public static void main(String[] args) {
        ServerRunner.run(HelloServer.class);
    }
}
