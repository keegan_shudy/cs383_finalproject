package utils;


import java.net.*;
import java.util.*;
import java.io.*;

import lejos.utility.Delay;
import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.hardware.Button;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.hardware.motor.*;


public class Client{
	private static Socket skt;
	protected static BufferedReader in;
	
	private Client(){
		throw new AssertionError();
	}
	
	/**
	 * used to establish a connection to the server robot
	 * 
	 * @param ip
	 *            - ip address of the server robot
	 * @param port
	 *            - port that the server robot has open
	 * @return BufferedReader for the established connection
	 */
	public static BufferedReader connect(String ip, int port) {
		try {
			//LCD.drawString("Running",0,4);
			System.out.println("Running");
			// Socket is an endpoint for communication between two machines
			// This robot connects to the server robot at the specified ip

			while (true) { // lol, the creativity here....
				try {
					skt = new Socket(ip, port);
					break;
				} catch (Exception e) {
				}
			}
			// BufferedReader reads from an input stream
			// InputStreamReader reads bytes and converts them to characters
			// getInputStream method allows to read the data from the socket
			in = new BufferedReader(new InputStreamReader(skt.getInputStream()));

			Sound.buzz();

			//LCD.drawString("Received string: ",0,5);
			Delay.msDelay(1000);

			// if the stream is not ready to be read, don't do anything, wait
			/*String message;
			while ((message = in.readLine()) != null) {
				LCD.drawString(message,0,6);
			}*/

			// System.out.println(in.readLine()); // Read one line and output
			// Delay.msDelay(3000);
			// Sound.buzz();
			// Delay.msDelay(3000);
			// System.out.print("\n");

			// Button.waitForAnyPress();
			// in.close();
			return in;

		} catch (Exception e) {
			System.out.print("Whoops! It didn't work!\n");
		}
		return null;
	}
	
	/*protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException,java.io.IOException{
		BufferedReader reader = request.getReader();
	    String str = "";
	    while ((str = reader.readLine()) != null)
	    {
	        System.out.println(str);
	    }

	    String p = request.getParameter("action");
	    //String input = request.getParameter("action"); 
	    System.out.print(p);
	
	}*/
	/*public String parseHTMLPage(String uri){
		
		try{
			URL url = new URL("http://"+uri);
			//URLConnection urlconnect = (URLConnection) url.openConnection();
			//InputStreamReader isr = new InputStreamReader(urlconnect.getInputStream());
			//in = new BufferedReader(isr);
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			String hold;
			while((hold=in.readLine()) != null){
				if(hold.contains("<label id=\"currently\">")){
					int start = hold.indexOf("<label id=\"currently\">");
					int end = hold.indexOf("</label>");
					System.out.println(hold.substring(start,end));
					return hold.substring(start,end);
				}
			}
			return ("unable to get the label and id");
			
		}catch(Exception e){return ("Something went wrong");}
		
	}*/
}