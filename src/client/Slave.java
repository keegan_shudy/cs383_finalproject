package client;

import utils.Client;

import server.SimpleWebServer;

import java.net.*;
import java.util.*;
import java.io.*;

import lejos.utility.Delay;
import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.hardware.Button;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.hardware.motor.*;


class Slave{
	private static final DifferentialPilot motors = new DifferentialPilot(5.6f,
			12.85f, Motor.A, Motor.C);
	
	
	public static void main(String [] argv){
		/*SimpleWebServer smp = new SimpleWebServer("141.195.226.86",80,
				                                 new File("/home/lejos/programs/www"),false);
		
		System.out.println("Press [Enter]");
		Button.ENTER.waitForPress();
		//Client c = new Client();
		//String action = c.parseHTMLPage("141.195.226.86");
		//while(Button.ESCAPE.isUp())
		BufferedReader brf=	doSomething();
		try{
		while(brf.readLine() != null)
			System.out.println(brf.readLine());
		}catch(IOException ioe) {System.out.println("io exception");}
		String action = "up";
		Delay.msDelay(10000);*/
		
		BufferedReader read = Client.connect("141.195.226.89",1235);
		String action = "", holder = "";
		int speed=60;
		int speedFactor=0;
		while(Button.ESCAPE.isUp()){
			try {
				action = read.readLine();
				
				System.out.println(action);
			} catch (Exception e0) {
				System.out.println("IOException");
			}
			if (!(action == "" || action == null || action == "\n")) {
				switch (action.toLowerCase()) {
				case "up":
					motors.travel(9999999, true);
					break;

				case "back":
					motors.travel(-9999999, true);
					break;

				case "left":
					motors.rotate(9999999, true);
					break;

				case "right":
					motors.rotate(-9999999, true);
					break;

				case "stop":
					motors.stop();
					break;

				
				case "plus":
					
					if (speed > 200)
						speed = 200;
					try {
						holder = read.readLine();
						holder.trim();
						if(holder.matches("-?\\d+(.\\d+)?"))
							speedFactor = Integer.parseInt(holder);
					} catch (Exception ex) {
						System.out.println(holder);
					}
					speed += speedFactor;
					motors.setTravelSpeed(speed);
					motors.setRotateSpeed(speed);
					System.out.println("Speed is now " + speed);
					break;

				case "minus":
					
					if (speed <= 5)
						speed = 5;

					try {
						holder = read.readLine();
						holder.trim();
						if(holder.matches("-?\\d+(.\\d+)?"))
							speedFactor = Integer.parseInt(holder);
					} catch (Exception ex) {
						System.out.println(holder);
					}
					speed -= speedFactor;
					motors.setTravelSpeed(speed);
					motors.setRotateSpeed(speed);
					System.out.println("Speed is now " + speed);
					break;

				default:
					System.out.println("Unknown");

				}
			}
		}
	}
	
	
	public static BufferedReader doSomething(){
		StringBuilder sb = new StringBuilder();
		sb.append("Hello there");
		try {
			URL url = new URL("http://141.195.226.86");
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoOutput(true);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "text/*");
			
			InputStream outputWriter = connection.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(
					outputWriter, "UTF-8"));
			System.out.println(br.readLine());
			return br;
			// br.flush();
			//br.close();
		} catch (IOException e) {
			System.out.println("Connection failed");
		}
		return null;
	}
	
}